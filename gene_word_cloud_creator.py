#!/usr/bin/python3
import argparse
import re
import csv

def parse_arguments():
    description = """
    A script to create a gene word cloud from a simple gene presence-absence ariba output file

    """
    parser = argparse.ArgumentParser(description=description,formatter_class=argparse.RawDescriptionHelpFormatter,)
    parser.add_argument('-f', '--ariba_output_filepath', help='path to the ariba output file',required=True)
    options = parser.parse_args()
    return options


def write_from_ariba_csv_file(ariba_filepath):
    reader = csv.DictReader(open(ariba_filepath,mode='r',newline=''))
    gene_data = dict()
    for row in reader:
        isolate_code = row['name'].split('.')[0]
        genes = list()
        for k,v in row.items(): #print (k,v)
            if v == 'yes' or v == 'yes_nonunique':
                #uncomment the next line if column titles end with ".assembled"
                #k = re.findall('(\S+).assembled',k)[0]
                #if k.endswith('_-'): k = re.findall('(\S+)_-+',k)[0]
                #elif k.endswith('_'): k = re.findall('(\S+)_+',k)[0]
                #elif k.endswith('-'): k = re.findall('(\S+)-+',k)[0]
                #else: pass
                k = k.replace(".match","")
                k = k.replace(".assembled","")
                k = k.replace("_","")
                k = k.replace("-","")
                genes.append(k)
            else: pass
        gene_data[isolate_code] = genes
    return gene_data

def compute_gene_word_count(gene_data):
    counts = dict()
    for isolate,genes in gene_data.items():
        genes = genes
        for gene in genes: 
            if gene == ' ':
                continue
            gene = gene.strip()
            #print(gene)
            counts[gene] = counts.get(gene,0) + 1
    sorted_counts = sorted(counts, key=counts.get, reverse=True)
    largest = None
    smallest = None
    for gene in sorted_counts:
        if largest is None or largest < counts[gene] :
            largest = counts[gene]
        if smallest is None or smallest > counts[gene]:
            smallest = counts[gene]
    # Spread the font sizes across 20-100 based on the count
    bigsize = 80
    smallsize = 20

    with open('gene_words.js','w') as f:
        f.write("gene_word = [")
        first_line = True
        for gene in sorted_counts:
            if not first_line : f.write( ",\n")
            first_line = False
            size = counts[gene]
            size = (size - smallest) / float(largest - smallest)
            size = int((size * bigsize) + smallsize)
            f.write("{text: '"+gene+"', size: "+str(size)+"}")
        f.write( "\n];\n")

    print("Output written to gene_words.js")
    return counts

def plot_gene_distribution(gene_counts):
    import matplotlib.pyplot as plt
    import numpy as np
    
    #sort in descending order
    gene_counts = {gene:count for (gene,count) in sorted(gene_counts.items(), key=lambda x: x[1], reverse=True)}
    #create x and y axis data fron gene_counts dictionary
    names = tuple([gene for gene,count in gene_counts.items()])
    counts = tuple([count for gene,count in gene_counts.items()])
    x_locations = len(gene_counts)
    ind = np.arange(x_locations)
    y_axis_max = max(gene_counts.values()) + 10
    
    #create plot
    plt.bar(ind, counts, width=0.5, color="blue", edgecolor="orange")
    plt.ylabel('Number of samples')
    plt.xlabel('Genes')
    plt.title('Gene distribution')
    plt.xticks(ind, names, rotation='vertical')
    plt.yticks(np.arange(0, y_axis_max, 5))
    plt.savefig("gene_distribution.png",bbox_inches='tight', transparent=True)
    plt.close()
    print("Output written to gene_distribution.png")


def write_visualization_file():
    #original htm file authored by Charles Severance --> https://www.py4e.com/code3/gmane/gword.htm
    htm_content = '''
    <!DOCTYPE html>
    <meta charset="utf-8">
    <script src="d3.v2.js"></script> <!--Source: https://d3js.org/d3.v2.js-->
    <script src="d3.layout.cloud.js"></script> <!--Source: https://www.npmjs.com/package/d3.layout.cloud/-->
    <script src="gene_words.js"></script>
    <body>
    <script>
    var fill = d3.scale.category20b();

    d3.layout.cloud().size([700, 700])
        .words(gene_word)
        .rotate(function() { return ~~(Math.random() * 2) * 90; })
        .font("Impact")
        .fontSize(function(d) { return d.size; })
        .on("end", draw)
        .start();

    function draw(words) {
        d3.select("body").append("svg")
            .attr("width", 700)
            .attr("height", 700)
        .append("g")
            .attr("transform", "translate(350,350)")
        .selectAll("text")
            .data(words)
        .enter().append("text")
            .style("font-size", function(d) { return d.size + "px"; })
            .style("font-family", "Impact")
            .style("fill", function(d, i) { return fill(i); })
            .attr("text-anchor", "middle")
            .attr("transform", function(d) {
            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            })
            .text(function(d) { return d.text; });
    }
    </script>
    '''
    with open('gene_word_cloud.htm','w') as f:
        f.write(htm_content)
    print("To see the visualization, copy the 'd3.layout.cloud.js' and 'd3.v2.js' files from the repository into the current working directory. \nAfterwards, open gene_word_cloud.htm in a browser.")


def main(ariba_output_filepath):
    gene_data = write_from_ariba_csv_file(ariba_output_filepath)
    gene_counts = compute_gene_word_count(gene_data)
    plot_gene_distribution(gene_counts)
    write_visualization_file()
        
if __name__ == "__main__":    
    options = parse_arguments()
    main(options.ariba_output_filepath)


