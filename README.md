# Gene Word Cloud Generator #
*Author: Erkison Odih - erkisonodih@gmail.com*
*Twitter: @bioinfo_erkison*

# Overview #
This script generates a word cloud and a bar chart showing the abundance and distribution of genes in a gene presence-absence file. 
The script leverages on the d3.js and the d3.layout.cloud.js frameworks for data visualization and uses a code snippet from Charles Severance' gword.htm code (https://www.py4e.com/code3/gmane/gword.htm). 

# Installation #
It is recommended that you install this within a bin directory that is available in your path. Simply clone this repository into your local machine and copy the needed files to your bin directory.  

```
cd some/directory/
git clone https://gitlab.com/bioinfo_erkison/gene-word-cloud-creator.git
cd gene_word_cloud_creator
cp gene_word_cloud_creator.py d3.v2.js d3.layout.cloud.js /some/bin/directory/available/in/your/path
```

# Usage #
The script takes one input - a gene presence-absence file formatted appropriately (Please see the 'tests' directory within the repository for a sample gene presence-absence file.
NOTE: This script was designed to create a gene word cloud from an ariba output file but would run an appropriately-formated gene-presence absence file as well.
Afterwards, run the command as follows:
```
gene_word_cloud_creator.py -f path/to/gene/presence-absence/file
```

# Viewing the output #
Two output files are generated
1. gene_distribution.png
2. gene_word_cloud.htm

Open the gene_word_cloud.htm file in a browser to view the visualization.
